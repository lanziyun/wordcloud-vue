# wordcloud

> A Vue.js project

## Build Setup

``` bash
#clone project
git clone https://lanziyun@bitbucket.org/lanziyun/wordcloud-vue.git

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run start

#store file沒有用到，因為我也是clone別人專案下來改的XD
#router file indes.js為執行檔


# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
