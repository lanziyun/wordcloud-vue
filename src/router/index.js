import Vue from 'vue'
import Router from 'vue-router'

// import HelloWorld from '@/components/HelloWorld'
import getUsers from '@/components/getUsers'
import addWords from '@/components/addWords'
import updateWords from '@/components/updateWords'
import login from '@/components/login'
import finish from '@/components/finish'
import WordCloud from '@/components/WordCloud'
import health from '@/components/health'
import home from '@/components/home'
import level from '@/components/level'
import levelup from '@/components/levelup'

//import VueCookies from 'vue-cookies'
//Vue.use(VueCookies)

Vue.use(require('vue-moment'));
Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/getUsers',
    //   name: 'getUsers',
    //   component: getUsers
    // },
    {
      path: '/addWords',
      name: 'addWords',
      component: addWords
    },
    {
      path: '/updateWords',
      name: 'updateWords',
      component: updateWords
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/finish',
      name: 'finish',
      component: finish
    },
    {
      path: '/WordCloud',
      name: 'WordCloud',
      component: WordCloud
    },
    {
      path: '/health',
      name: 'health',
      component: health
    },
    {
      path: '/home',
      name: 'home',
      component: home
    },
    {
      path: '/level',
      name: 'level',
      component: level
    },
    {
      path: '/levelup',
      name: 'levelup',
      component: levelup
    },
    {
      path: '*', //當路徑為其它不存在的路徑時
      redirect: '/login' //重新導向此路徑(可自己定義)
    },
  ]
})

